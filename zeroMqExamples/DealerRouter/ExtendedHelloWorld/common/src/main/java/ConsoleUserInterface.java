import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class ConsoleUserInterface {

    private Scanner scanner;

    public ConsoleUserInterface() {
        scanner = new Scanner(System.in);
    }

    public String getUserInput(String[] args, int argIndex, String prompt, String defaultResponse) {
        String response = "";

        if (args!=null && args.length>argIndex)
            response = args[argIndex];
        else {
            System.out.print(String.format("%s (default=%s): ", prompt, defaultResponse));
            response = scanner.nextLine();
            if (response == null || response.isEmpty())
                response = defaultResponse;
        }

        return response;
    }

    public void getContinuationResponse(String prompt, String[] expectedResponses) {
        if (expectedResponses==null)
            expectedResponses = new String[0];

        String response = null;
        while (response==null || !isAnExpectedResponse(expectedResponses, response)) {
            System.out.print(prompt);
            response = scanner.nextLine();
        }
    }

    private boolean isAnExpectedResponse(String[] expectedResponses, String response) {
        if (expectedResponses.length==0) return true;

        boolean result = false;
        for (String expected : expectedResponses)
            if (expected.toLowerCase().trim().equals(response.toLowerCase().trim()))
            {
                result = true;
                break;
            }

        return result;
    }
}
