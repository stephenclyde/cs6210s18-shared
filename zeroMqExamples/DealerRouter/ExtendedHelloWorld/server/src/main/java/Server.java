import org.zeromq.ZContext;

public class Server {

    public static void main(String[] args) throws Exception {

        ZContext zmqContext = new ZContext(1);

        ConsoleUserInterface ui = new ConsoleUserInterface();

        String proxyHost = ui.getUserInput(args, 0,"Enter proxy host", "localhost");
        String serverName = ui.getUserInput(args, 1,"Enter server name", "X");

        ServerWorker server = new ServerWorker(zmqContext, proxyHost, serverName);
        server.start();

        ui.getContinuationResponse("Type EXIT or X at anytime to exit: ", new String[] {"EXIT", "X"});

        server.stop();
        zmqContext.close();
    }

}
