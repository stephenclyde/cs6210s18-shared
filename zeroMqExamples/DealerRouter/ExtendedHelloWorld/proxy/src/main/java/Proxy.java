import org.zeromq.ZContext;

import java.util.Scanner;

public class Proxy {
    public static void main(String[] args) throws Exception {
        ZContext zmqContext = new ZContext(1);

        ProxyWorker proxy = new ProxyWorker(zmqContext);
        proxy.start();

        ConsoleUserInterface ui = new ConsoleUserInterface();
        ui.getContinuationResponse("Type EXIT or X at anytime to exit: ", new String[] {"EXIT", "X"});

        proxy.stop();
        zmqContext.close();
    }
}
