import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class Subscriber {

    public static void main(String[] args) {

        ZContext context = new ZContext(1);

        ZMQ.Socket socket = context.createSocket(ZMQ.SUB);
        socket.subscribe("".getBytes());
        socket.connect("tcp://localhost:26001");

        int messageCount = 0;
        boolean keepGoing = true;
        while (keepGoing)
        {
            String message = socket.recvStr();
            if (message.startsWith("Terminate"))
                break;

            System.out.println(message);
            messageCount++;
        }


        // Display reply
        System.out.println(String.format("Received %d messages", messageCount));

        context.destroySocket(socket);
        context.close();
    }
}
