import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class Publisher {

    public static void main(String[] args) throws InterruptedException {

        ZContext context = new ZContext(1);

        ZMQ.Socket socket = context.createSocket(ZMQ.PUB);
        socket.bind("tcp://*:26001");

        // Wait a while for connections to be established
        Thread.sleep(1000);

        System.out.println("Start sending out messages");

        // Publish a whole lot of numbers
        for (int i=0; i<100000; i++) {
            // Generate some message to publish
            String message = String.format("Number: %d", i);
            socket.send(message);
        }

        // Sends an terminate message
        socket.send("Terminate");

        // Display reply
        System.out.println("Done sending out messages");

        context.destroySocket(socket);
        context.close();
    }
}
