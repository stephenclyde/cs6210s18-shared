import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.util.Random;

public class Client implements Runnable {

    private Thread thread;
    private boolean keepGoing;

    private String serverHost;
    private String firstName;
    private String lastName;
    private PossibleLanguages preferredLanguage;
    private ZContext context;
    private ZMQ.Socket socket;
    private Random random = new Random();

    Client(ZContext context, String serverHost, String firstName, String lastName, PossibleLanguages preferredLanguage) {
        this.context = context;
        this.serverHost = serverHost;
        this.firstName = firstName;
        this.lastName = lastName;
        this.preferredLanguage = preferredLanguage;
    }

    public void start() {
        if (keepGoing) return;

        keepGoing = true;
        thread = new Thread(this, "HelloWorldClient");
        thread.start();
    }

    public void stop() throws InterruptedException {
        if (!keepGoing) return;

        keepGoing = false;
        while (thread!=null)
            Thread.sleep(10);
    }

    @Override
    public void run() {
        initialize();

        while (keepGoing && !Thread.currentThread().isInterrupted())
        {
            if (random.nextDouble() < 0.8) continue;

            // Make a request
            HelloRequest request = new HelloRequest(firstName, lastName, preferredLanguage);
            Envelope requstEnv = new Envelope(request);
            socket.send(requstEnv.getPackagedMessage());

            // Wait for a reply
            byte[] replyBytes = socket.recv(0);
            if (replyBytes==null || replyBytes.length==0)
            {
                System.out.println("No response from server -- reconnect and try again");
                initialize();
            }

            // Display reply
            Envelope replyEnv = new Envelope(replyBytes);
            if (replyEnv.getMessage().getClass() == HelloReply.class) {
                HelloReply reply = (HelloReply) replyEnv.getMessage();
                System.out.println(reply.getGreeting());
                System.out.print("Current Client Names: ");
                String allNames = String.join(", ", reply.getClientNames());
                System.out.println(allNames);
            }

            // Take a break
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        context.destroySocket(socket);
        socket = null;
        thread = null;

    }

    private void initialize() {
        if (socket!=null)
            context.destroySocket(socket);

        socket = context.createSocket(ZMQ.REQ);
        socket.setReceiveTimeOut(1000);
        socket.setLinger(0);
        String endPoint = String.format("tcp://%s:26001", serverHost);
        socket.connect(endPoint);
    }

}
