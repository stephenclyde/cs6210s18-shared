import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class Program {

    public static void main(String[] args) {

        ZContext context = new ZContext(1);

        ZMQ.Socket socket = context.createSocket(ZMQ.REP);
        socket.bind("tcp://*:26001");

        while (!Thread.currentThread().isInterrupted())
        {
            // Get a request
            String clientName = socket.recvStr(0);

            // Do some work
            System.out.println(String.format("Received Hello from %s", clientName));

            // Send reply back to client
            String reply = String.format("Hello, %s", clientName);
            socket.send(reply, 0);
        }

        context.destroySocket(socket);
        context.close();
    }
}
