import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class Program {
    public static void main(String[] args) {

        ZContext context = new ZContext(1);

        ZMQ.Socket socket = context.createSocket(ZMQ.REQ);
        socket.connect("tcp://localhost:26001");

        // Make a request
        socket.send("Bob");
        System.out.println("Greeting sent");

        // Wait for a reply
        String reply = socket.recvStr(0);

        // Display reply
        System.out.println(reply);
        context.close();
    }
}
