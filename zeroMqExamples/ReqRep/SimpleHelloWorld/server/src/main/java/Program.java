import org.zeromq.ZContext;

public class Program {

    public static void main(String[] args) throws Exception {

        ZContext zmqContext = new ZContext(1);

        Server server = new Server(zmqContext);
        server.start();

        System.out.println("Type ENTER at anytime to exit");
        System.in.read();

        server.stop();
        zmqContext.close();
    }

}
