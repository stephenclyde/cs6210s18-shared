import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.util.Random;

public class Client implements Runnable {

    private Thread thread;
    private boolean keepGoing;

    private String serverHost;
    private String username;
    private ZContext context;
    private ZMQ.Socket socket;
    private Random random = new Random();

    Client(ZContext context, String serverHost, String username) {
        this.context = context;
        this.serverHost = serverHost;
        this.username = username;
    }

    public void start() {
        if (keepGoing) return;

        keepGoing = true;
        thread = new Thread(this, "HelloWorldClient");
        thread.start();
    }

    public void stop() throws InterruptedException {
        if (!keepGoing) return;

        keepGoing = false;
        while (thread!=null)
            Thread.sleep(10);
    }

    @Override
    public void run() {
        initialize();

        while (keepGoing && !Thread.currentThread().isInterrupted())
        {
            if (random.nextDouble() < 0.8) continue;

            // Make a request
            socket.send(username);

            // Wait for a reply
            String reply = socket.recvStr(0);
            if (reply==null)
            {
                System.out.println("No response from server -- stopping");
                keepGoing = false;
                break;
            }

            // Display reply
            System.out.println(reply);

            // Take a break
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        context.destroySocket(socket);
        socket = null;
        thread = null;

    }

    private void initialize() {
        socket = context.createSocket(ZMQ.REQ);
        socket.setReceiveTimeOut(1000);
        socket.setLinger(0);
        String endPoint = String.format("tcp://%s:26001", serverHost);
        System.out.println(String.format("Connecting to %s", endPoint));
        socket.connect(endPoint);
    }

}
