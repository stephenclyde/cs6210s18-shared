﻿using System.Runtime.Serialization;

namespace S3Example
{
    [DataContract]
    public class Widget
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public double Height { get; set; }
        [DataMember]
        public double Length { get; set; }
        [DataMember]
        public double Weight { get; set; }

    }
}
