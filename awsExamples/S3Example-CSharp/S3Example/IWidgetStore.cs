﻿namespace S3Example
{
    public interface IWidgetStore
    {
        bool Save(Widget widget);
        Widget Load(string id);
    }
}
