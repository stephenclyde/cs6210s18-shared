﻿using Amazon;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;

namespace S3Example
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var chain = new CredentialProfileStoreChain();
            AWSCredentials awsCredentials;
            if (!chain.TryGetAWSCredentials("default", out awsCredentials)) return;
    
            CredentialProfile awsProfile;
            if (!chain.TryGetProfile("default", out awsProfile)) return;

            if (awsProfile.Region==null)
                awsProfile.Region = RegionEndpoint.USEast1;
        
            var store = new AwsWidgetStore()
            {
                Credentials = awsCredentials,
                Profile = awsProfile
            };

            var widget = new Widget()
            {
                Id = "widget1",
                Description = "First Widget",
                Height = 10.3,
                Length = 13.5,
                Weight = 235.2
            };

            store.Save(widget);

            var widget2 = store.Load("widget1");

            if (widget2!=null)
                System.Console.WriteLine("Loaded Widget from S3");
        }
    }
}
