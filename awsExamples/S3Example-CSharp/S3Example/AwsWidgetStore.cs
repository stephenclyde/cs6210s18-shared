﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.S3;
using Amazon.S3.Model;

namespace S3Example
{
    public class AwsWidgetStore : IWidgetStore
    {
        private static readonly DataContractJsonSerializer JsonSerializer = new DataContractJsonSerializer(typeof(Widget));

        public AWSCredentials Credentials { get; set; }
        public CredentialProfile Profile { get; set; }

        public bool Save(Widget widget)
        {
            var result = false;

            var stream = new MemoryStream();
            JsonSerializer.WriteObject(stream, widget);

            using (IAmazonS3 client = new AmazonS3Client(Credentials, Profile.Region))
            {
                try
                {
                    // simple object put
                    var request = new PutObjectRequest()
                    {
                        ContentBody = Encoding.ASCII.GetString(stream.ToArray()),
                        BucketName = "usu.cs6210",
                        Key = widget.Id
                    };

                    var response = client.PutObject(request);
                    result = (response.HttpStatusCode == HttpStatusCode.OK);
                }
                catch (AmazonS3Exception amazonS3Exception)
                {
                    if (amazonS3Exception.ErrorCode != null &&
                        (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                         amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                    {
                        Console.WriteLine("Please check the provided AWS Credentials.");
                        Console.WriteLine(
                            "If you haven't signed up for Amazon S3, please visit http://aws.amazon.com/s3");
                    }
                    else
                    {
                        Console.WriteLine("An error occurred with the message '{0}' when writing an object",
                            amazonS3Exception.Message);
                    }
                }
                finally
                {
                    stream.Close();
                }
            }

            return result;
        }

        public Widget Load(string id)
        {
            Widget widget = null;

            using (IAmazonS3 client = new AmazonS3Client(Credentials, Profile.Region))
            {
                try
                {
                    var request = new GetObjectRequest()
                    {
                        BucketName = "usu.cs6210",
                        Key = id
                    };

                    using (var response = client.GetObject(request))
                    {
                        widget = JsonSerializer.ReadObject(response.ResponseStream) as Widget;
                    }
                }
                catch (AmazonS3Exception amazonS3Exception)
                {
                    if (amazonS3Exception.ErrorCode != null &&
                        (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                        amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                    {
                        Console.WriteLine("Please check the provided AWS Credentials.");
                        Console.WriteLine("If you haven't signed up for Amazon S3, please visit http://aws.amazon.com/s3");
                    }
                    else
                    {
                        Console.WriteLine($"An error occurred with the message '{amazonS3Exception.Message}' when reading an object");
                    }
                }

            }

            return widget;
        }
    }
}
